package com.jaeger.connpod.util;

public class LogUtil {

	private static final String DATA_DELIMITER = "||";

	public String generateLogText(long processTimeMillis, String... additionalData) {
		StringBuilder sb = new StringBuilder();
		sb.append(" timeMillis:[" + processTimeMillis + "] data:");

		for (int i = 0; i < additionalData.length; i++) {
			sb.append(additionalData[i]);
			if (i < additionalData.length - 1) {
				sb.append(DATA_DELIMITER);
			}
		}

		return sb.toString();
	}

}
