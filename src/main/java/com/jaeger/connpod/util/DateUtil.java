package com.jaeger.connpod.util;

public class DateUtil extends org.apache.commons.lang3.time.DateUtils {

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DATE_TIME_FORMAT_WITH_MILLISECOND = "yyyy-MM-dd'T'HH:mm:ss.SSS";

}
