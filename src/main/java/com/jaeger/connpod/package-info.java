/**
 * <code>conn-pod</code> is common library for all jaeger projects.
 * 
 * @author timpamungkas
 *
 */
package com.jaeger.connpod;