package com.jaeger.connpod.rest.builder;

import java.util.List;

import com.jaeger.connpod.rest.json.JsonBaseError;
import com.jaeger.connpod.rest.json.JsonBaseHeader;
import com.jaeger.connpod.rest.json.JsonBaseResponse;

public class JsonResponseBuilder<T> {

	private JsonBaseResponse<T> result;
	private JsonBaseHeader header;

	public JsonResponseBuilder() {
		this.result = new JsonBaseResponse<>();
		this.header = new JsonBaseHeader();

		this.result.setHeader(header);
	}

	public JsonResponseBuilder<T> addError(JsonBaseError error) {
		this.header.addError(error);

		return this;
	}

	public JsonResponseBuilder<T> addError(String message, String reason, String code) {
		JsonBaseError error = new JsonBaseError();
		error.setCode(code);
		error.setMessage(message);
		error.setReason(reason);

		this.header.addError(error);

		return this;
	}

	public JsonBaseResponse<T> build() {
		return result;
	}

	public JsonResponseBuilder<T> withData(T dataContent) {
		this.result.setContent(dataContent);

		return this;
	}

	public JsonResponseBuilder<T> withErrors(List<JsonBaseError> errors) {
		this.header.setErrors(errors);

		return this;
	}

	public JsonResponseBuilder<T> withProcessTimeMillis(long processTimeMillis) {
		this.header.setProcessTimeMillis(processTimeMillis);

		return this;
	}

	public JsonResponseBuilder<T> withSuccess(boolean successFlag) {
		this.header.setSuccess(successFlag);

		return this;
	}

}
