package com.jaeger.connpod.rest.json;

import java.util.Arrays;

public class JsonBaseResponse<T> {

	private JsonBaseHeader header;

	private T data;

	public JsonBaseResponse() {
		super();
	}
	
	public JsonBaseResponse(long startTime, boolean success, JsonBaseError...errors) {
		this();
		
		JsonBaseHeader jsonBaseHeader = new JsonBaseHeader();
		jsonBaseHeader.setSuccess(success);		
		jsonBaseHeader.setErrors(Arrays.asList(errors));		
		jsonBaseHeader.setProcessTimeMillis(System.currentTimeMillis() - startTime);
		this.setHeader(jsonBaseHeader);
	}
	
	public T getData() {
		return data;
	}

	public JsonBaseHeader getHeader() {
		return header;
	}

	public void setContent(T data) {
		this.data = data;
	}

	public void setHeader(JsonBaseHeader header) {
		this.header = header;
	}

}
