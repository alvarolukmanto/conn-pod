package com.jaeger.connpod.rest.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonBaseHeader {

	@JsonProperty(value = "process_time")
	private long processTimeMillis;
	private boolean success;
	private List<JsonBaseError> errors = new ArrayList<>();

	public void addError(JsonBaseError error) {
		this.errors.add(error);
	}

	public List<JsonBaseError> getErrors() {
		return errors;
	}

	public long getProcessTimeMillis() {
		return processTimeMillis;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setErrors(List<JsonBaseError> errors) {
		this.errors = errors;
	}

	public void setProcessTimeMillis(long processTimeMillis) {
		this.processTimeMillis = processTimeMillis;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
