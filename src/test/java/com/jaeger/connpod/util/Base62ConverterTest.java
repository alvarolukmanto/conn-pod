package com.jaeger.connpod.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.jaeger.connpod.util.Base62Converter;

class Base62ConverterTest {

	private static final long[] base10 = { 2147483649l, 125l, 7912l, 2799574l, 7l, 746954085l, 2147483647l,
			7988206810683l, 1978447680636l };

	// use this to get the value : https://www.dcode.fr/base-n-convert
	private static final List<int[]> base62 = List.of(new int[] { 2, 21, 20, 38, 37, 3 }, new int[] { 2, 1 },
			new int[] { 2, 3, 38 }, new int[] { 11, 46, 18, 26 }, new int[] { 7 }, new int[] { 50, 34, 8, 54, 33 },
			new int[] { 2, 21, 20, 38, 37, 1 }, new int[] { 2, 16, 39, 30, 5, 43, 23, 37 },
			new int[] { 34, 51, 34, 60, 52, 21, 54 });

	private static final long PERFORMANCE_MILLIS_TRESHOLD = 5;

	private static Base62Converter base62Converter = new Base62Converter();

	@Test
	@BeforeAll
	static final void testInitializer() {
		assertEquals(base10.length, base62.size());
		assertNotNull(base62Converter);
	}

	private String buildBase62String(int[] position) {
		StringBuilder sb = new StringBuilder();

		for (int i : position) {
			sb.append(Base62Converter.BASE62_CHARS.charAt(i));
		}

		return sb.toString();
	}

	@Test
	final void testFromBase10() {
		for (int i = 0; i < base10.length; i++) {
			var base62StringActual = base62Converter.fromBase10(base10[i]);
			var base62StringExpected = buildBase62String(base62.get(i));

			assertEquals(base62StringExpected, base62StringActual);
		}
	}

	@Test
	final void testFromBase10Performance() {
		for (int i = 0; i < base10.length; i++) {
			final var index = i;

			assertTimeoutPreemptively(Duration.ofMillis(PERFORMANCE_MILLIS_TRESHOLD), () -> {
				@SuppressWarnings("unused")
				var base62String = base62Converter.fromBase10(base10[index]);
			});
		}
	}

	@Test
	final void testToBase10() {
		for (int i = 0; i < base62.size(); i++) {
			var base62String = buildBase62String(base62.get(i));
			var base10Num = base62Converter.toBase10(base62String);

			assertEquals(base10[i], base10Num);
		}
	}

	@Test
	final void testToBase10Performance() {
		for (int i = 0; i < base62.size(); i++) {
			final var base62String = buildBase62String(base62.get(i));

			assertTimeoutPreemptively(Duration.ofMillis(PERFORMANCE_MILLIS_TRESHOLD), () -> {
				@SuppressWarnings("unused")
				var base10Num = base62Converter.toBase10(base62String);
			});
		}
	}

}
