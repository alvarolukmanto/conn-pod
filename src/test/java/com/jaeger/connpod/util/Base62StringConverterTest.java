package com.jaeger.connpod.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.jaeger.connpod.util.Base62StringConverter;

class Base62StringConverterTest {

	private static final String[] originalString = { "Hello World", "1800275", "A pretty girl on a busy street",
			"0x952", "90gk2iiv9", "jaeger", "Jaeger", "1", "29", "5980092675", "498jn4ab3386",
			"A quick brown bear jumps over a lazy pink pig", "f" };

	private static final String[] base62EncodedString = { "73XpUgyMwkGr29M", "11S0NatIcH",
			"uA2aOzSYFvjBG4UlCM8V2ipBStj8xiEWUPLo0FpU", "3fEQx6Y", "KGwWfC0gVzbd", "XDGDhTaE", "NDou1pVi", "n", "3LN",
			"1Fu5BF2zyopcxB", "L1ERoH2w19eD6uve", "1hyWGBKXauVgu3sfeplGk7NRd4TCqOnOWlEAfFbSrn8hHwyA3p9dye7RBxuYJ",
			"1e" };

	private static final long PERFORMANCE_MILLIS_TRESHOLD = 5;

	private static Base62StringConverter base62StringConverter = new Base62StringConverter();

	@Test
	@BeforeAll
	static final void testInitializer() {
		assertEquals(originalString.length, base62EncodedString.length);
		assertNotNull(base62StringConverter);
	}

	@Test
	final void testDecode() {
		for (int i = 0; i < base62EncodedString.length; i++) {
			var encoded = base62StringConverter.decode(base62EncodedString[i].getBytes());
			assertEquals(originalString[i], encoded);
		}
	}

	@Test
	final void testDecodePerformance() {
		for (int i = 0; i < base62EncodedString.length; i++) {
			final var index = i;

			assertTimeoutPreemptively(Duration.ofMillis(PERFORMANCE_MILLIS_TRESHOLD), () -> {
				@SuppressWarnings("unused")
				var original = base62StringConverter.decode(base62EncodedString[index].getBytes());
			});
		}
	}

	@Test
	final void testEncode() {
		for (int i = 0; i < originalString.length; i++) {
			var encoded = base62StringConverter.encode(originalString[i].getBytes());
			assertEquals(base62EncodedString[i], encoded);
		}
	}

	@Test
	final void testEncodePerformance() {
		for (int i = 0; i < originalString.length; i++) {
			final var index = i;

			assertTimeoutPreemptively(Duration.ofMillis(PERFORMANCE_MILLIS_TRESHOLD), () -> {
				@SuppressWarnings("unused")
				var encoded = base62StringConverter.encode(originalString[index].getBytes());
			});
		}
	}

}
